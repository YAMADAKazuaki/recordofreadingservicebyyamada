import React from 'react';
import {withRouter, Link} from 'react-router-dom';
import Header from './Header'
import BookRow from './BookRow'
import BookRegister from './BookRegister'

class Books extends React.Component {
    constructor(props) {
        super(props)
        this.state = {userid: '', bookRows:[]}

        this.handleUserChanged = this.handleUserChanged.bind(this)
        this.handleDeleteBookRow = this.handleDeleteBookRow.bind(this)
        this.listenerOnloadGetBookRows = this.listenerOnloadGetBookRows.bind(this)
        this.handleAddBookRow = this.handleAddBookRow.bind(this)
        this.handleUserPass = this.handleUserPass.bind(this)
    }
    
    componentDidMount() {
        this.getBooks()
    }

    handleDeleteBookRow(e) {
        this.getBooks()
    }

    handleAddBookRow(e) {
        this.getBooks()
    }

    handleUserChanged(user) {
        this.setState({userid: user.id})
    }

    handleUserPass() {
        return this.state.userid
    }

    getBooks() {
        var xhrPost = new XMLHttpRequest()
        xhrPost.onload = this.listenerOnloadGetBookRows
        xhrPost.open('POST', 'http://localhost:8081/book/findAll')
        xhrPost.setRequestHeader('Pragma', 'no-cache')
        xhrPost.setRequestHeader('Cache-Control', 'no-cache')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {userid: this.state.userid}
        xhrPost.send(JSON.stringify(j));
    }

    listenerOnloadGetBookRows(e) {
        if (e.target.status == 401) {
          this.props.history.replace('/')
          return
        }
        let res = JSON.parse(e.target.response)
        this.setState({bookRows: res})
    }

    render() {
        const rows = this.state.bookRows.map((element, index) =>
        <BookRow value={element} key={element.id} onDeleteBookRow={this.handleDeleteBookRow} />
        )
        return (
            <div>
                <Header onUserChanged={this.handleUserChanged} />
                <h3>書籍登録</h3>
                <BookRegister userid={this.state.userid} onBookAddRow={this.handleAddBookRow} onUserPass={this.handleUserPass}/>
                <hr />
                <h3>書籍一覧</h3>
                {rows}
            </div>
        )
    }
}
export default withRouter(Books)