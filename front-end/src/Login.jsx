import React from 'react';
import {withRouter, Link} from 'react-router-dom';

class Login extends React.Component{
  constructor(props) {
    super(props);

    this.state = {email: '', password: ''};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.listenerOnLoad = this.listenerOnLoad.bind(this);
  }

  handleSubmit(e){
    e.preventDefault();
    var xhrPost = new XMLHttpRequest()
    xhrPost.onload = this.listenerOnLoad
    xhrPost.open('POST', 'http://localhost:8081/user/login')
    xhrPost.setRequestHeader('Pragma', 'no-cache')
    xhrPost.setRequestHeader('Cache-Control', 'no-cache')
    var body = {email: this.state.email, password: this.state.password};
    xhrPost.send(JSON.stringify(body));  
  }

  handleEmailChange(e) {
    if (e.target && e.target.value) {
      this.setState({email: e.target.value})
    }
  }

  handlePasswordChange(e) {
    if (e.target && e.target.value) {
      this.setState({password: e.target.value});
    }
  }

  listenerOnLoad(e) {
    if (e.target.status >= 400) {
      if (e.target.response == 'password wrong\n') {
        window.alert('このEmail, パスワードの組み合わせは存在しません。')
        return
      }
      if (e.target.response == 'is not verified\n') {
        window.alert('Emailの検証が済んでいません。')
        return
      }
      window.alert('ログインに失敗しました。');
    } else {
      var token = e.target.getResponseHeader('Authorization');
      sessionStorage.setItem('token',token)
      this.props.history.push('/books')
    }
  }

  render() {
    return (
      <div>
        <div className="main">
          <h1>ログイン</h1>
          <form onSubmit={this.handleSubmit}>
            <input type="text" placeholder="email" onChange={ this.handleEmailChange}/>
            <input type="password" placeholder="password" onChange={ this.handlePasswordChange}/>
            <div style={{textAlign:"cener"}}>
              <button type="submit" className="btn btn-default">ログイン</button>
            </div>
          </form>
        </div>
        <Link to='/signup'>ユーザ登録へ</Link>
      </div>
    );
  }};

  export default withRouter(Login)