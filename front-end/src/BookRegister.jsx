import React from 'react';
import {withRouter, Link} from 'react-router-dom';

class BookRegister extends React.Component {
    constructor(props) {
        super(props)
        this.state = {userid: '', title: '', author: '', image: '', isbn13: '', suggestions: [], selected:false}

        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleAuthorChange = this.handleAuthorChange.bind(this)
        this.handleISBN13Change = this.handleISBN13Change.bind(this)

        this.listenerOnloadHandleClick = this.listenerOnloadHandleClick.bind(this)
        this.listenerOnloadGetSuggestions = this.listenerOnloadGetSuggestions.bind(this)
    }

    handleClick(e) {
        e.preventDefault();
        var userid = this.props.onUserPass()

        var xhrPost = new XMLHttpRequest()
    
        xhrPost.onload = this.listenerOnloadHandleClick
        xhrPost.open('POST', 'http://localhost:8081/book/add')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {
            userid: userid,
            title: this.state.title,
            author: this.state.author,
            image: this.state.image,
            isbn13: this.state.isbn13
        };
        xhrPost.send(JSON.stringify(j));
    }

    handleChange(e) {
        const value = e.target.value
        if (value.length > 0) {
            let xhrGet = new XMLHttpRequest()
            xhrGet.onload = this.listenerOnloadGetSuggestions
            xhrGet.open('POST', 'http://localhost:8081/book/suggestions')
            xhrGet.setRequestHeader('Pragma', 'no-cache')
            xhrGet.setRequestHeader('Cache-Control', 'no-cache')
            let j = {title: value}
            xhrGet.send(JSON.stringify(j));
        }
        if (this.state.selected) {
            this.setState({author: '', image: '', isbn13: '', selected: false})
        }
        this.setState({title: value})
    }

    handleAuthorChange(e) {
        this.setState({author: e.target.value})
    }

    handleISBN13Change(e) {
        this.setState({isbn13: e.target.value})
    }

    listenerOnloadHandleClick(e) {
        if (e.target.status == 400 && e.target.response == "title is empty\n") {
            window.alert("タイトルを入力してください。");
            return
        }
        if (e.target.status == 401) {
          this.props.history.replace('/')
          return
        }
        this.props.onBookAddRow()
    }

    listenerOnloadGetSuggestions(e) {
        let res = JSON.parse(e.target.response)
        this.setState({suggestions: res})
    }

    selectedText(value) {
        this.setState({title: value.title, author: value.author, image: value.image, isbn13: value.isbn13, suggestions: [], selected: true})
    }

    renderSuggestions() {
        let { suggestions } = this.state;
        if(suggestions.length === 0){
            return null;
        }
        return (
            <div>
                <p>候補選択</p>
                <ul >
                    {
                        suggestions.map((item, index) => (<li key={index} onClick={() => this.selectedText(item)}>{item.title}</li>))
                    }
                </ul>
            </div>
        );
    }

    render() {
        return (
            <div className="center-block">
                <form className="form-horizontal">
                    <div className="form-group">
                    <div className="control-label col-sm-2"></div>
                    </div>
                    <div className="form-group">
                        <div className="control-label col-sm-1">タイトル: </div>
                        <div className="col-sm-10">
                            <input type="text" onChange={this.handleChange} value={this.state.title}/><br />
                        </div>
                    </div>
                    {this.renderSuggestions()}
                    <div className="form-group">
                        <div className="control-label col-sm-1">著者:</div>
                        <div className="col-sm-10">
                            <input type="text" onChange={this.handleAuthorChange} value={this.state.author} /><br />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="control-label col-sm-1">ISBN13: </div>
                        <div className="col-sm-10">
                            <input type="text" onChange={this.handleISBN13Change} value={this.state.isbn13}/><br />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="control-label col-sm-1"></div>
                        <div className="col-sm-10">
                            <input type="button" onClick={this.handleClick} value="登録" className="btn btn-default"/>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(BookRegister)