import React from 'react';
import {withRouter, Link} from 'react-router-dom'

class BookRow extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            id: this.props.value.id,
            userid: this.props.value.userid,
            title: this.props.value.title,
            author: this.props.value.author,
            image: this.props.value.image,
            isbn13: this.props.value.isbn13,
            edit: false
        }
    
        this.handleChange = this.handleChange.bind(this)
        this.handleAuthorChange = this.handleAuthorChange.bind(this)
        this.handleISBN13Change = this.handleISBN13Change.bind(this)

        this.handleSaveClick = this.handleSaveClick.bind(this)
        this.handleEditClick = this.handleEditClick.bind(this)
        this.handleDeleteClick = this.handleDeleteClick.bind(this)
        this.handleEditClick = this.handleEditClick.bind(this)
        this.handleMemoClick = this.handleMemoClick.bind(this)

        this.listenerOnLoadSaveClick = this.listenerOnLoadSaveClick.bind(this)
        this.listenerOnLoadGetBookRow = this.listenerOnLoadGetBookRow.bind(this)
        this.listenerOnLoadDeleteClick = this.listenerOnLoadDeleteClick.bind(this)
    }

    handleChange(e) {
        this.setState({title: e.target.value})
    }

    handleAuthorChange(e) {
        this.setState({author: e.target.value})
    }

    handleISBN13Change(e) {
        this.setState({isbn13: e.target.value})
    }

    handleSaveClick(e) {
        e.preventDefault()
        var xhrPost = new XMLHttpRequest()
    
        xhrPost.onload = this.listenerOnLoadSaveClick
        xhrPost.open('POST', 'http://localhost:8081/book/update')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {
            id: this.state.id,
            title: this.state.title,
            author: this.state.author,
            image: this.state.image,
            isbn13: this.state.isbn13
        };
        xhrPost.send(JSON.stringify(j));

        this.setState({edit: false})
    }

    handleEditClick(event) {
        event.preventDefault();
        this.setState({edit: true});
    }

    handleDeleteClick(event) {
        event.preventDefault()
        if (window.confirm("削除しますか？")) {
            var xhrDelete = new XMLHttpRequest()

            xhrDelete.onload = this.listenerOnLoadDeleteClick
            xhrDelete.open('DELETE', 'http://localhost:8081/book/delete')
            xhrDelete.setRequestHeader('Authorization', sessionStorage.getItem('token'))
            var j = {id: this.state.id};
            xhrDelete.send(JSON.stringify(j));
        }
    }

    handleMemoClick(e) {
        var stateToPass = {
            userid: this.state.userid,
            book: {
                id: this.state.id,
                author: this.state.author,
                title: this.state.title,
                isbn13: this.state.isbn13,
                image: this.state.image
            }
        }
        this.props.history.push({
            pathname: '/memos', state: stateToPass
        })
    }

    getBookRow() {
        var xhrPost = new XMLHttpRequest()
        xhrPost.onload = this.listenerOnLoadGetBookRow
        xhrPost.open('POST', 'http://localhost:8081/book/find')
        xhrPost.setRequestHeader('Pragma', 'no-cache')
        xhrPost.setRequestHeader('Cache-Control', 'no-cache')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {id: this.state.id};
        xhrPost.send(JSON.stringify(j));
    }

    listenerOnLoadGetBookRow(e) {
        var res = JSON.parse(e.target.response)
        this.setState({title: res.title, author: res.author, image: res.image, isbn13: res.isbn13})
    }

    listenerOnLoadSaveClick(e) {
        this.getBookRow()
    }

    listenerOnLoadDeleteClick(e) {
        this.props.onDeleteBookRow()
    }

    render() {
        var stateToPass = {
            userid: this.state.userid,
            book: {
                id: this.state.id,
                author: this.state.author,
                title: this.state.title,
                isbn13: this.state.isbn13,
                image: this.state.image
            }
        }
        return (
            <div>
                <div className="form-horizontal">
                    <div className="form-group">
                        <div className="col-sm-offset-1">
                            <img src={this.state.image} />
                            <Link to={{pathname: '/memos', state: stateToPass}}>{this.state.title}</Link>
                        </div>
                    </div>
                    <div className="btn-group pull-right btn-upper">
                        <button type="button" value="update" onClick={this.handleMemoClick} className="btn btn-default btn-upper">メモ一覧</button>
                        <button type="button" value="delete" onClick={this.handleDeleteClick} className="btn btn-default btn-upper">削除</button>
                    </div>
                </div>
                <hr />
            </div>
        )
    }
}
export default withRouter(BookRow)

