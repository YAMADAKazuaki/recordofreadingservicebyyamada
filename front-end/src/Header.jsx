import React from 'react'
import {withRouter} from 'react-router-dom'
import { Navbar, Nav, NavDropdown} from 'react-bootstrap';

class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {user: {name: ''}}
    this.handleClick = this.handleClick.bind(this)

    this.listenerOnloadHandleLogoutClick = this.listenerOnloadHandleLogoutClick.bind(this)
    this.listenerOnloadGetUser = this.listenerOnloadGetUser.bind(this)
  }

  componentDidMount() {
    this.getUser()
  }

  handleClick(e){
    var xhrPost = new XMLHttpRequest()
    xhrPost.onload = this.listenerOnloadHandleLogoutClick
    xhrPost.open('POST', 'http://localhost:8081/user/logout')
    xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
    xhrPost.send();
  }

  getUser() {
    var xhrGet = new XMLHttpRequest()
    xhrGet.onload = this.listenerOnloadGetUser
    xhrGet.open('GET', 'http://localhost:8081/user/findFromSession')
    xhrGet.setRequestHeader('Pragma', 'no-cache')
    xhrGet.setRequestHeader('Cache-Control', 'no-cache')
    xhrGet.setRequestHeader('Authorization', sessionStorage.getItem('token'))
    xhrGet.send();
  }
  
  listenerOnloadHandleLogoutClick(e) {
    this.props.history.replace('/')
  }

  listenerOnloadGetUser(e) {
    if (e.target.status == 401) {
      this.props.history.replace('/')
      window.alert('ログインしてください。')
      return
    }
    var res = JSON.parse(e.target.response)
    this.setState({user: res})
    this.props.onUserChanged(res)
  }

  render() {
    return (
      <header>
        <Navbar bg="dark"  variant="dark">
          <Navbar.Brand href="#home">
            <img alt="" src="/public/image/book_icon.png" width="30" height="30" className="d-inline-block align-top" />{' '}
            読書メモ
          </Navbar.Brand>
          <Navbar.Collapse id="responsive-navbar-nav" className="">
            <Nav className="justify-content-end pull-right">
              <NavDropdown className="dropdown-menu-right" title={this.state.user.name} id="nav-dropdown">
                <NavDropdown.Item eventKey="4.1">環境設定</NavDropdown.Item>
                <NavDropdown.Item eventKey="4.2">ヘルプ</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item eventKey="4.4" onClick={this.handleClick}>ログアウト</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    );
  }
}

export default withRouter(Header);