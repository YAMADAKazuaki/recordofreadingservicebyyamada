'use strict';

import React from 'react';
import {withRouter} from 'react-router-dom';

class MemoRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.value.id,
            markdown: this.props.value.markdown,
            html: this.props.value.html,
            edit: false};

        this.handleTextAreaChange = this.handleTextAreaChange.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
    }

    handleTextAreaChange(event) {
        this.setState({markdown: event.target.value});
    }

    handleSaveClick(event) {
        event.preventDefault();
        var xhrPost = new XMLHttpRequest()
    
        xhrPost.addEventListener('load', () => {
          this.getMemoRow(this.state.id)
        })
        xhrPost.open('POST', 'http://localhost:8081/memo/update')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {id: this.state.id, markdown: this.state.markdown};
        xhrPost.send(JSON.stringify(j));

        this.setState({edit: false})
    }

    handleEditClick(event) {
        event.preventDefault();
        this.setState({edit: true});
    }

    handleDeleteClick(event) {
        event.preventDefault()
        if (window.confirm("削除しますか？")) {
            var xhrDelete = new XMLHttpRequest()

            xhrDelete.addEventListener('load', () => {
                this.props.onDeleteMemoRow()
            })
            xhrDelete.open('DELETE', 'http://localhost:8081/memo/delete')
            xhrDelete.setRequestHeader('Authorization', sessionStorage.getItem('token'))
            var j = {id: this.state.id};
            xhrDelete.send(JSON.stringify(j));
        }
    }

    getMemoRow(id) {
        var xhrPost = new XMLHttpRequest()
        xhrPost.addEventListener('load', () => {
          var res = JSON.parse(xhrPost.responseText)
          this.setState({html: res.html})
        })
        xhrPost.open('POST', 'http://localhost:8081/memo/find')
        xhrPost.setRequestHeader('Pragma', 'no-cache')
        xhrPost.setRequestHeader('Cache-Control', 'no-cache')
        xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
        var j = {id: this.state.id};
        xhrPost.send(JSON.stringify(j));
    }

    createMarkup(html){
        return {__html: html};
    };
    
    render() {
        if (this.state.edit) {
            return (
                <form onSubmit={this.handleSaveClick}>
                    <textarea className="markdown" value={this.state.markdown} cols="50" rows="10" onChange={this.handleTextAreaChange} />
                    <input type="submit" value="保存" className="btn btn-default" />
                </form>
            )
        } else {
            return (
                <div>
                    <form >
                        <span dangerouslySetInnerHTML={this.createMarkup(this.state.html) }/>
                        <div className="button-group pull-right btn-upper">
                            <button type="submit" value="update" onClick={this.handleEditClick} className="btn btn-default btn-upper">変更</button>
                            <button type="submit" value="delete" onClick={this.handleDeleteClick} className="btn btn-default btn-upper">削除</button>
                        </div>
                    </form>
                    <hr />
                </div>
            )

        }
    }
}

export default withRouter(MemoRow)