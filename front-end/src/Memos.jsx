'use strict';

import React from 'react';
import {withRouter, Link} from 'react-router-dom'
import MarkdownField from './MarkdownField';
import MemoRow from './MemoRow';
import Header from './Header'

class Memos extends React.Component {
  constructor(props) {
    super(props);

    if (!props.location.state || !props.history.location.state.userid) {
      this.props.history.replace("/")
      this.state = {userid: '', book: {id: '', title: '', author: '', isbn13: '', image: ''}, markdown: '', memoRows: []}
      return
    }
    this.state = {
      userid: props.location.state.userid,
      book: props.location.state.book,
      markdown: '',
      memoRows: []
    };

    this.handleAddClick = this.handleAddClick.bind(this);
    this.handleMarkdownFieldChange = this.handleMarkdownFieldChange.bind(this);
    this.handleDeleteMemoRow = this.handleDeleteMemoRow.bind(this);
    this.handleUserChanged = this.handleUserChanged.bind(this)

    this.listenerOnloadGetMemoRows = this.listenerOnloadGetMemoRows.bind(this);
    this.listenerOnloadHandleAddClick = this.listenerOnloadHandleAddClick.bind(this)
  }

  handleMarkdownFieldChange(md) {
    this.setState({markdown: md})
  }

  componentDidMount() {
    this.getMemoRows()
  }

  getMemoRows() {
    var xhrPost = new XMLHttpRequest()
    xhrPost.onload = this.listenerOnloadGetMemoRows
    xhrPost.open('POST', 'http://localhost:8081/memo/findAll')
    xhrPost.setRequestHeader('Pragma', 'no-cache')
    xhrPost.setRequestHeader('Cache-Control', 'no-cache')
    xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
    var j = {userid: this.state.userid, bookid: this.state.book.id}
    xhrPost.send(JSON.stringify(j));
  }

  handleAddClick(e) {
    e.preventDefault();
    var xhrPost = new XMLHttpRequest()

    xhrPost.onload = this.listenerOnloadHandleAddClick
    xhrPost.open('POST', 'http://localhost:8081/memo/add')
    xhrPost.setRequestHeader('Authorization', sessionStorage.getItem('token'))
    var j = {
      userid: this.state.userid,
      bookid: this.state.book.id,
      markdown: this.state.markdown
    };
    xhrPost.send(JSON.stringify(j));
  }

  handleDeleteMemoRow() {
    this.getMemoRows()
  }

  handleUserChanged() {
  }

  listenerOnloadGetMemoRows(e) {
    if (e.target.status == 401) {
      window.alert('ログインしてください。')
      this.props.history.replace('/')
      return
    }
    if (e.target.status == 400 && e.target.response == "bookid is empty\n") {
      window.alert('本を選択してください。')
      this.props.history.replace('/')
      return
    }
    var res = JSON.parse(e.target.response)
    this.setState({memoRows: res})
  }

  listenerOnloadHandleAddClick(e) {
    if (e.target.status == 401) {
      this.props.history.replace('/')
      return
    }
    if (e.target.status === 400 && e.target.response === "content is empty\n") {
      window.alert("内容を入力してください。空のメモは登録できません。")
    }
    this.getMemoRows()
  }

  render() {
    const rows = this.state.memoRows.map((element, index) =>
      <MemoRow value={element} key={element.id} onDeleteMemoRow={this.handleDeleteMemoRow} />
    )
    return (
      <div className="memos">
        <Header onUserChanged={this.handleUserChanged}/>
        <div>
          <img src={this.state.book.image} />{this.state.book.title}  
        </div>
        <form onSubmit={this.handleAddClick} >
          <MarkdownField value={this.state.markdown} onMarkdownFieldChange={this.handleMarkdownFieldChange}/>
          <input type="submit" value="メモを追加" className="btn btn-default"/>
        </form>
        <hr />
        {rows}
        <Link to="/books">書籍一覧へ</Link>
      </div>
      )
    }
  }

export default withRouter(Memos)