import React from 'react';
import {withRouter, Link} from 'react-router-dom';

class Signup extends React.Component{
  constructor(props) {
    super(props);

    this.state = {name: '', email: '', password: ''};

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.listenerOnLoad = this.listenerOnLoad.bind(this);
  }

  handleSubmit(e){
    e.preventDefault();
    var xhrPost = new XMLHttpRequest()
    xhrPost.onload = this.listenerOnLoad
    xhrPost.open('POST', 'http://localhost:8081/user/signup')
    xhrPost.setRequestHeader('Pragma', 'no-cache')
    xhrPost.setRequestHeader('Cache-Control', 'no-cache')
    var body = {name: this.state.name, email: this.state.email, password: this.state.password};
    xhrPost.send(JSON.stringify(body));  
  }

  handleNameChange(e) {
    if (e.target && e.target.value) {
      this.setState({name: e.target.value})
    }
  }

  handleEmailChange(e) {
    if (e.target && e.target.value) {
      this.setState({email: e.target.value})
    }
  }

  handlePasswordChange(e) {
    if (e.target && e.target.value) {
      this.setState({password: e.target.value});
    }
  }

  listenerOnLoad(e) {
    if (e.target.status >= 400) {
      if (e.target.response == 'already registered\n') {
        window.alert('このEmailは既に登録されています。')
        return 
      }
      if (e.target.response == 'fill parameter\n') {
        window.alert('名前、Email, パスワードを全て入力してください。')
        return
      }
      if (e.target.response == 'invalid email address\n') {
        window.alert('メールアドレスが不正です。有効なメールアドレスを入力してください。')
        return
      }
      window.alert('登録に失敗しました。');
    } else {
      this.props.history.push('/')
    }
  }

  render() {
    return (
      <div>
        <div className="main">
          <h1>ユーザ登録</h1>
          <form onSubmit={this.handleSubmit}>
            <input type="text" placeholder="name" onChange={ this.handleNameChange}/>
            <input type="text" placeholder="email" onChange={ this.handleEmailChange}/>
            <input type="password" placeholder="password" onChange={ this.handlePasswordChange}/>
            <div style={{textAlign:"cener"}}>
              <button type="submit" className="btn btn-default">登録</button>
            </div>
          </form>
        </div>
        <Link to='/'>ログインへ</Link>
      </div>
    );
  }};

  export default withRouter(Signup)