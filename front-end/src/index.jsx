
import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './Login';
import Memos from './Memos';
import Signup from './Signup';
import Books from './Books'

function Routes() {
  return <Switch>
    <Route exact path={"/"}><Login /></Route>
    <Route path={"/signup"}><Signup /></Route>
    <Route path={"/books"}><Books /></Route>
    <Route path={"/memos"}><Memos /></Route>
  </Switch>
};

ReactDOM.render(
  <Router><Routes /></Router>,
  document.getElementById('root')
);