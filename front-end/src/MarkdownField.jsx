'use strict';

import React from 'react';

const e = React.createElement;

class MarkdownField extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.onMarkdownFieldChange(event.target.value);
  }

  render() {
    return (<textarea className="markdown" cols="50" rows="10" value={this.props.markdown} onChange={this.handleChange} />)
  }
}

export default MarkdownField