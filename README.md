Gmailを使ったログイン認証を実装したら、特定のIP以外からのアクセスを禁止された。。。<br />
別手段を検討しようとしないまま放置している。<br />
そもそもローカル実行以外の手段（固定文字列でlocalhostを参照していた）を考慮していないサンプル実装だった。。

# record of reading with markdown

## FrontEnd ##

### install dependencies ###
```shell
$ cd front-end
$ npm install
```

### build ###
```shell
$ cd front-end
$ ./node_modules/.bin/webpack
```
distにbundle.jsが作成される


## BackEnd ##

### build ###
```shell
$ cd back-end
$ go build
```

### middleware ###
dockerを起動しておく。

```shell
$ cd docker
$ docker-compose up -d
```

### 実行 ###

```shell
$ cd back-end
$ ./recordofreadingservicebyyamada
```

### URL(port) ###
http://localhost:8081/

### markdown ###

```text
- a
	-b
```
↓

- a
	- b

### 実行後

```shell
$ cd docker
$ docker-compose down
```
