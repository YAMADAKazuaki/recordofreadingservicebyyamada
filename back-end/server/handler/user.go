package handler

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/service"
)

func getUserRequest(r *http.Request) model.UserRequest {
	body := r.Body
	requstJSON, err := ioutil.ReadAll(body)
	if err != nil {
		panic(err)
	}
	defer body.Close()

	var jsn model.UserRequest
	if err := json.Unmarshal(requstJSON, &jsn); err != nil {
		panic(err)
	}
	reader := bytes.NewReader(requstJSON)
	io.Copy(os.Stdout, reader)

	return jsn
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	userRepo := getUserRepository()
	tokenRepo := getTokenRepository()
	jsn := getUserRequest(r)

	token, err := service.Login(userRepo, tokenRepo, jsn.Email, jsn.Password)
	if err != nil {
		handleError(w, err)
		return
	}

	w.Header().Add("Authorization", "Bearer: "+token.ID)
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	user, err := getUserFromSession(w, r)
	if err != nil {
		http.Error(w, "Unauthorized", 401)
	}
	repo := getTokenRepository()

	service.Logout(repo, user.ID)
}

func SignUpHandler(w http.ResponseWriter, r *http.Request) {
	userRepo := getUserRepository()
	userMailRepo := getUserMailRepository()
	jsn := getUserRequest(r)

	err := service.SignUp(userRepo, userMailRepo, jsn)
	if err != nil {
		handleError(w, err)
	}
}

func VerifyHandler(w http.ResponseWriter, r *http.Request) {
	repo := getUserRepository()

	q := r.URL.Query()
	var userid string
	var token string
	for key, value := range q {
		if key == "userid" {
			userid = value[0]
		}
		if key == "token" {
			token = value[0]
		}
	}
	var err error
	if len(userid) > 0 && len(token) > 0 {
		err = service.Vefiry(repo, userid, token)
	}
	if err != nil {
		handleError(w, err)
		return
	}
	w.Write([]byte("メールは認証されました。"))

}

func FindUserFromSessionHandler(w http.ResponseWriter, r *http.Request) {
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}
	userResponse := model.UserResponse{
		ID:       user.ID,
		Name:     user.Name,
		Password: "",
		Email:    user.Email}
	resbody, err := json.Marshal(userResponse)
	if err != nil {
		panic(err)
	}
	res := bytes.NewReader(resbody)
	multiWriter := io.MultiWriter(os.Stdout, w)
	io.Copy(multiWriter, res)
}
