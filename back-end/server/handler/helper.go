package handler

import (
	"net/http"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/repository"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/service"
	infrastructure_http "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/infrastructure/http"
	infrastructure_mail "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/infrastructure/mail"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/infrastructure/mongo"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/util"
)

func getBookRepository() repository.BookRepository {
	return &mongo.BookMongoRepository{}
}

func getBookHTTPRepository() repository.BookHTTPRepository {
	return &infrastructure_http.BookHTTPGoogleRepository{}
}

func getMemoRepository() repository.MemoRepository {
	return &mongo.MemoMongoRepository{}
}

func getUserRepository() repository.UserRepository {
	return &mongo.UserMongoRepository{}
}

func getUserMailRepository() repository.UserMailRepository {
	return &infrastructure_mail.UserEmailGmailRepository{}
}

func getTokenRepository() repository.TokenRepository {
	return &mongo.TokenMongoRepository{}
}

func getUserFromSession(w http.ResponseWriter, r *http.Request) (model.UserResponse, error) {
	auth := r.Header.Get("Authorization")
	tokenRepo := getTokenRepository()
	userRepo := getUserRepository()

	return service.GetUserFromSession(userRepo, tokenRepo, auth)
}

func handleError(w http.ResponseWriter, err error) {
	switch err.(type) {
	case *util.ClientError:
		e := err.(*util.ClientError)
		http.Error(w, err.Error(), e.Status)
	case *util.ServerError:
		e := err.(*util.ServerError)
		http.Error(w, err.Error(), e.Status)
	default:
		http.Error(w, "Internal Server Error", 500)
	}
}
