package handler

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/service"
)

func getMemoRequest(r *http.Request) model.MemoRequest {
	body := r.Body
	requstJSON, err := ioutil.ReadAll(body)
	if err != nil {
		panic(err)
	}
	defer body.Close()

	var jsn model.MemoRequest
	if err := json.Unmarshal(requstJSON, &jsn); err != nil {
		panic(err)
	}
	reader := bytes.NewReader(requstJSON)
	io.Copy(os.Stdout, reader)

	return jsn
}

func FindMemoHandler(w http.ResponseWriter, r *http.Request) {
	repo := getMemoRepository()
	jsn := getMemoRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	memoResponses := service.FindMemo(repo, jsn.ID)

	resbody, err := json.Marshal(memoResponses)
	if err != nil {
		panic(err)
	}
	res := bytes.NewReader(resbody)
	multiWriter := io.MultiWriter(os.Stdout, w)
	io.Copy(multiWriter, res)
}

func FindAllMemosHandler(w http.ResponseWriter, r *http.Request) {
	repo := getMemoRepository()
	jsn := getMemoRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	memoResponses, err := service.FindAllMemos(repo, jsn.UserID, jsn.BookID)
	if err != nil {
		handleError(w, err)
		return
	}

	resbody, err := json.Marshal(memoResponses)
	if err != nil {
		panic(err)
	}
	res := bytes.NewReader(resbody)
	//multiWriter := io.MultiWriter(os.Stdout, w)
	//io.Copy(multiWriter, res)
	io.Copy(w, res)
}

func AddMemoHandler(w http.ResponseWriter, r *http.Request) {
	repo := getMemoRepository()
	jsn := getMemoRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	err = service.AddMemo(repo, jsn.Markdown, user.ID, jsn.BookID)
	if err != nil {
		handleError(w, err)
	}
}

func UpdateMemoHandler(w http.ResponseWriter, r *http.Request) {
	repo := getMemoRepository()
	jsn := getMemoRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	service.UpdateMemo(repo, jsn.ID, jsn.Markdown)
}

func DeleteMemoHandler(w http.ResponseWriter, r *http.Request) {
	repo := getMemoRepository()
	jsn := getMemoRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	service.DeleteMemo(repo, jsn.ID)
}
