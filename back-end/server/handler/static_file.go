package handler

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/public/index.html")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}

func BundleJsHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/dist/bundle.js")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}

func PrismCSSHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/public/stylesheet/prism.css")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}

func PrismJsHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/public/javascript/prism.js")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}

func MainCSSHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/public/stylesheet/main.css")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}

func BookIconPNGHandler(w http.ResponseWriter, r *http.Request) {
	file, err := ioutil.ReadFile("../front-end/public/image/book_icon.png")
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	reader := bytes.NewReader(file)
	io.Copy(w, reader)
}
