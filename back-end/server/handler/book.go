package handler

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/service"
)

func getBookRequest(r *http.Request) model.BookRequest {
	body := r.Body
	requstJSON, err := ioutil.ReadAll(body)
	if err != nil {
		panic(err)
	}
	defer body.Close()

	var jsn model.BookRequest
	if err := json.Unmarshal(requstJSON, &jsn); err != nil {
		panic(err)
	}
	reader := bytes.NewReader(requstJSON)
	io.Copy(os.Stdout, reader)

	return jsn
}

func FindBookHandler(w http.ResponseWriter, r *http.Request) {
	repo := getBookRepository()
	jsn := getBookRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	bookResponse := service.FindBook(repo, jsn.ID)

	resbody, err := json.Marshal(bookResponse)
	if err != nil {
		panic(err)
	}
	res := bytes.NewReader(resbody)
	multiWriter := io.MultiWriter(os.Stdout, w)
	io.Copy(multiWriter, res)
}

func FindAllBooksHandler(w http.ResponseWriter, r *http.Request) {
	repo := getBookRepository()
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	bookResponses := service.FindAllBooks(repo, user.ID)

	resbody, err := json.Marshal(bookResponses)
	if err != nil {
		panic(err)
	}
	res := bytes.NewReader(resbody)
	//multiWriter := io.MultiWriter(os.Stdout, w)
	//io.Copy(multiWriter, res)
	io.Copy(w, res)
}

func AddBookHandler(w http.ResponseWriter, r *http.Request) {
	repo := getBookRepository()
	jsn := getBookRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	err = service.AddBook(repo, jsn)
	if err != nil {
		handleError(w, err)
	}
}

func UpdateBookHandler(w http.ResponseWriter, r *http.Request) {
	repo := getBookRepository()
	jsn := getBookRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	service.UpdateBook(repo, jsn)
}

func DeleteBookHandler(w http.ResponseWriter, r *http.Request) {
	bookRepo := getBookRepository()
	memoRepo := getMemoRepository()
	jsn := getBookRequest(r)
	user, err := getUserFromSession(w, r)
	if len(user.ID) == 0 || err != nil {
		http.Error(w, "Unauthorized", 401)
		return
	}

	service.DeleteBook(bookRepo, memoRepo, user.ID, jsn.ID)
}

func SuggestionsBookHandler(w http.ResponseWriter, r *http.Request) {
	repo := getBookHTTPRepository()
	jsn := getBookRequest(r)

	bookResponses := service.SuggestionsBook(repo, jsn)
	resbody, err := json.Marshal(bookResponses)
	if err != nil {
		panic(err)
	}

	res := bytes.NewReader(resbody)
	//multiWriter := io.MultiWriter(os.Stdout, w)
	//io.Copy(multiWriter, res)
	io.Copy(w, res)
}
