module gitlab.com/yamadakazuaki/recordofreadingservicebyyamada

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
)
