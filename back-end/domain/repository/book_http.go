package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type BookHTTPRepository interface {
	SuggestionsBook(book model.BookRecord) []model.BookRecord
}
