package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type UserRepository interface {
	FindByID(id string) model.UserRecord
	FindByEmail(email string) model.UserRecord
	Add(record model.UserRecord)
	Verify(id string) error
}
