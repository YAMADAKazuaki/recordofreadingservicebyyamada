package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type UserMailRepository interface {
	SendMail(user model.UserRecord)
}
