package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type MemoRepository interface {
	Find(id string) model.MemoRecord
	FindAll(userid string, bookid string) []model.MemoRecord
	Add(md string, userid string, bookid string)
	Update(id string, md string)
	Delete(id string)
}
