package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type TokenRepository interface {
	FindByID(id string) model.TokenRecord
	FindByUserID(userID string) model.TokenRecord
	Add(userID string)
	DeleteByUserID(userID string)
}
