package repository

import "gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"

type BookRepository interface {
	Find(id string) model.BookRecord
	FindAll(userid string) []model.BookRecord
	Add(book model.BookRecord)
	Update(book model.BookRecord)
	Delete(id string)
}
