package model

type MemoRequest struct {
	ID       string `json:"id"`
	UserID   string `json:"userid"`
	BookID   string `json:"bookid"`
	Markdown string `json:"markdown"`
}

type MemoRecord struct {
	ID              string `bson:"_id"`
	UserID          string `bson:"userid"`
	BookID          string `bson:"bookid"`
	Markdown        string `bson:"markdown"`
	CreatedDateTime string `bson:"created_datetime"`
}

type MemoResponse struct {
	ID              string `json:"id"`
	UserID          string `json:"userid"`
	BookID          string `json:"bookid"`
	HTML            string `json:"html"`
	Markdown        string `json:"markdown"`
	CreatedDateTime string `json:"created_datetime"`
}
