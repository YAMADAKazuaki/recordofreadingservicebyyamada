package model

type TokenRecord struct {
	ID              string `bson:"_id"`
	UserID          string `bson:"userid"`
	CreatedDateTime string `bson:"created_datetime"`
}

type TokenResponse struct {
	ID              string `json:"id"`
	UserID          string `json:"userid"`
	CreatedDateTime string `json:"created_datetime"`
}
