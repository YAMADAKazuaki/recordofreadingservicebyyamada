package model

type UserRequest struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type UserRecord struct {
	ID         string `bson:"_id"`
	Name       string `bson:"name"`
	Password   string `bson:"password"`
	Email      string `bson:"email"`
	IsVerified bool   `bson:"is_verified"`
	Token      string `bson:"token"`
}

type UserResponse struct {
	ID         string `json:"id"`
	Name       string `json:"name"`
	Password   string `json:"password"`
	Email      string `json:"email"`
	IsVerified bool   `json:"is_verified"`
}
