package model

type BookRequest struct {
	ID     string `json:"id"`
	UserID string `json:"userid"`
	Title  string `json:"title"`
	Author string `json:"author"`
	Image  string `json:"image"`
	ISBN13 string `json:"isbn13"`
}

type BookRecord struct {
	ID     string `bson:"_id"`
	UserID string `bson:"userid"`
	Title  string `bson:"title"`
	Author string `bson:"author"`
	Image  string `bson:"image"`
	ISBN13 string `bson:"isbn13"`
}

type BookResponse struct {
	ID     string `json:"id"`
	UserID string `json:"userid"`
	Title  string `json:"title"`
	Author string `json:"author"`
	Image  string `json:"image"`
	ISBN13 string `json:"isbn13"`
}

type BookSuggestion struct {
	Items []BookItem `json:"items"`
}

type BookItem struct {
	VolumnInfo VolumeInfo `json:"volumeInfo"`
}

type VolumeInfo struct {
	Title               string               `json:"title"`
	Authors             []string             `json:"authors"`
	ImageLinks          ImageLinks           `json:"imageLinks"`
	IndustryIdentifiers []IndustryIdentifier `json:"industryIdentifiers"`
}

type ImageLinks struct {
	SmallThumnailURL string `json:"smallThumbnail"`
	ThumnailURL      string `json:"thunmnail"`
}
type IndustryIdentifier struct {
	Type       string `json:"type"`
	Identifier string `json:"identifier"`
}

//var NoImageURL = "https://upload.wikimedia.org/wikipedia/ja/b/b5/Noimage_image.png"
var NoImageURL = "https://upload.wikimedia.org/wikipedia/commons/4/41/Noimage.svg"
