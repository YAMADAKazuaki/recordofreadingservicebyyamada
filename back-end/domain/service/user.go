package service

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"regexp"
	"time"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/repository"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/util"
)

func Login(userRepo repository.UserRepository, tokenRepo repository.TokenRepository, email string, password string) (model.TokenResponse, error) {
	userRow := userRepo.FindByEmail(email)
	pw := sha256.Sum256([]byte(password))
	pwString := hex.EncodeToString(pw[:])
	if pwString != userRow.Password {
		e := &util.ClientError{Msg: "password wrong", Status: 400}
		return model.TokenResponse{}, e
	}
	if len(userRow.Email) == 0 {
		e := &util.ClientError{Msg: "email is empty", Status: 400}
		return model.TokenResponse{}, e
	}
	if !userRow.IsVerified {
		e := &util.ClientError{Msg: "is not verified", Status: 400}
		return model.TokenResponse{}, e
	}
	tokenRepo.DeleteByUserID(userRow.ID)
	tokenRepo.Add(userRow.ID)
	token := tokenRepo.FindByUserID(userRow.ID)
	response := model.TokenResponse{
		ID:              token.ID,
		UserID:          token.UserID,
		CreatedDateTime: token.CreatedDateTime,
	}
	return response, nil
}

func Logout(repo repository.TokenRepository, userID string) {
	repo.DeleteByUserID(userID)
}

func SignUp(userRepo repository.UserRepository, UserEmailRepo repository.UserMailRepository, user model.UserRequest) error {
	if len(user.Name) == 0 || len(user.Email) == 0 || len(user.Password) == 0 {
		return &util.ClientError{Msg: "fill parameter", Status: 400}
	}
	r := regexp.MustCompile("[^\\s]+@[^\\s]+")
	if !r.MatchString(user.Email) {
		return &util.ClientError{Msg: "invalid email address", Status: 400}
	}
	token := sha256.Sum256([]byte(user.Name + time.Now().String()))
	password := sha256.Sum256([]byte(user.Password))

	ur := userRepo.FindByEmail(user.Email)
	if len(ur.Email) > 0 {
		return &util.ClientError{Msg: "already registered", Status: 400}
	}
	userRecord := model.UserRecord{
		ID:         user.ID,
		Name:       user.Name,
		Password:   hex.EncodeToString(password[:]),
		Email:      user.Email,
		IsVerified: false,
		Token:      hex.EncodeToString(token[:]),
	}
	userRepo.Add(userRecord)
	userRecord = userRepo.FindByEmail(user.Email)
	UserEmailRepo.SendMail(userRecord)
	return nil
}

func Vefiry(repo repository.UserRepository, userID string, token string) error {
	user := repo.FindByID(userID)
	if len(user.ID) == 0 {
		return &util.ClientError{Msg: "invalid userid", Status: 400}
	}
	if user.IsVerified {
		return &util.ClientError{Msg: "already verified", Status: 400}
	}
	if user.Token == token {
		err := repo.Verify(userID)
		if err != nil {
			return err
		}
	} else {
		return &util.ClientError{Msg: "invalid token", Status: 400}
	}
	return nil
}

func GetUserFromSession(userRepo repository.UserRepository, tokenRepo repository.TokenRepository, authorization string) (model.UserResponse, error) {
	var token string
	fmt.Sscanf(authorization, "Bearer: %s", &token)
	fmt.Println("token = " + token)
	if len(token) == 0 {
		return model.UserResponse{}, &util.ClientError{Msg: "empty token", Status: 400}
	}
	t := tokenRepo.FindByID(token)
	if len(t.ID) == 0 {
		return model.UserResponse{}, &util.ClientError{Msg: "invalid token", Status: 400}
	}

	userRow := userRepo.FindByID(t.UserID)
	response := model.UserResponse{
		ID:       userRow.ID,
		Name:     userRow.Name,
		Email:    userRow.Email,
		Password: "",
	}
	return response, nil
}
