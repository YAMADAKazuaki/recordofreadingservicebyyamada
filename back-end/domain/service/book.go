package service

import (
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/repository"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/util"
)

func FindBook(repo repository.BookRepository, id string) model.BookResponse {
	bookRow := repo.Find(id)
	response := model.BookResponse{
		ID:     bookRow.ID,
		UserID: bookRow.UserID,
		Title:  bookRow.Title,
		Author: bookRow.Author,
		Image:  bookRow.Image,
		ISBN13: bookRow.ISBN13}
	return response
}

func FindAllBooks(repo repository.BookRepository, userID string) []model.BookResponse {
	rows := repo.FindAll(userID)
	responses := make([]model.BookResponse, 0)
	for _, row := range rows {
		response := model.BookResponse{
			ID:     row.ID,
			UserID: row.UserID,
			Title:  row.Title,
			Author: row.Author,
			Image:  row.Image,
			ISBN13: row.ISBN13}
		responses = append(responses, response)
	}
	return responses
}

func AddBook(repo repository.BookRepository, book model.BookRequest) error {
	if len(book.Title) == 0 {
		return &util.ClientError{
			Msg:    "title is empty",
			Status: 400,
		}
	}
	var image string
	if len(book.Image) > 0 {
		image = book.Image
	} else {
		image = model.NoImageURL
	}
	record := model.BookRecord{
		UserID: book.UserID,
		Title:  book.Title,
		Author: book.Author,
		Image:  image,
		ISBN13: book.ISBN13,
	}
	repo.Add(record)

	return nil
}

func UpdateBook(repo repository.BookRepository, book model.BookRequest) {
	record := model.BookRecord{
		ID:     book.ID,
		UserID: book.UserID,
		Title:  book.Title,
		Author: book.Author,
		Image:  book.Image,
		ISBN13: book.ISBN13,
	}
	repo.Update(record)
}

func DeleteBook(bookRepo repository.BookRepository, memoRepo repository.MemoRepository, userID string, bookID string) {
	memoRecords := memoRepo.FindAll(userID, bookID)
	for _, memoRecord := range memoRecords {
		memoRepo.Delete(memoRecord.ID)
	}
	bookRepo.Delete(bookID)
}

func SuggestionsBook(repo repository.BookHTTPRepository, book model.BookRequest) []model.BookResponse {
	b := model.BookRecord{
		Title: book.Title,
	}
	suggestions := repo.SuggestionsBook(b)
	var responses []model.BookResponse
	for _, suggestion := range suggestions {
		response := model.BookResponse{
			ID:     suggestion.ID,
			UserID: suggestion.UserID,
			Title:  suggestion.Title,
			Author: suggestion.Author,
			Image:  suggestion.Image,
			ISBN13: suggestion.ISBN13,
		}
		responses = append(responses, response)
	}

	return responses
}
