package service

import (
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/repository"
	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/util"
)

func FindMemo(repo repository.MemoRepository, id string) model.MemoResponse {
	memoRecord := repo.Find(id)
	html := mdToHTML([]byte(memoRecord.Markdown))
	transformedRow := model.MemoResponse{
		UserID:          memoRecord.UserID,
		BookID:          memoRecord.BookID,
		ID:              memoRecord.ID,
		HTML:            string(html),
		Markdown:        memoRecord.Markdown,
		CreatedDateTime: memoRecord.CreatedDateTime}
	return transformedRow
}

func FindAllMemos(repo repository.MemoRepository, userID string, bookID string) ([]model.MemoResponse, error) {
	if len(userID) == 0 {
		return nil, &util.ClientError{
			Msg:    "userid is empty",
			Status: 401,
		}
	}
	if len(bookID) == 0 {
		return nil, &util.ClientError{
			Msg:    "bookid is empty",
			Status: 400,
		}
	}
	memoRecords := repo.FindAll(userID, bookID)
	memoResponses := make([]model.MemoResponse, 0)
	for _, memoRecord := range memoRecords {
		html := mdToHTML([]byte(memoRecord.Markdown))
		memoResponse := model.MemoResponse{
			UserID:          memoRecord.UserID,
			BookID:          memoRecord.BookID,
			ID:              memoRecord.ID,
			HTML:            string(html),
			Markdown:        memoRecord.Markdown,
			CreatedDateTime: memoRecord.CreatedDateTime}
		memoResponses = append(memoResponses, memoResponse)
	}
	return memoResponses, nil
}

func AddMemo(repo repository.MemoRepository, markdown string, userID string, bookID string) error {
	if len(markdown) == 0 {
		return &util.ClientError{
			Msg:    "content is empty",
			Status: 400,
		}
	}
	repo.Add(markdown, userID, bookID)
	return nil
}

func UpdateMemo(repo repository.MemoRepository, ID string, markdown string) {
	repo.Update(ID, markdown)
}

func DeleteMemo(repo repository.MemoRepository, ID string) {
	repo.Delete(ID)
}
