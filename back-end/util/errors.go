package util

type ClientError struct {
	Msg    string
	Status int
}

type ServerError struct {
	Msg    string
	Status int
}

func (err *ClientError) Error() string {
	return err.Msg
}

func (err *ServerError) Error() string {
	return err.Msg
}
