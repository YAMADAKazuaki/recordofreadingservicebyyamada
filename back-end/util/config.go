package util

type Config struct {
	Server ServerConfig
	Db     DbConfig
}

type ServerConfig struct {
	Host         string `toml:"host"`
	Port         int64  `toml:"port"`
	RootUserName string `toml:"rootusername"`
	RootPassword string `toml:"rootpassword"`
}

type DbConfig struct {
	Database string `toml:"database"`
}
