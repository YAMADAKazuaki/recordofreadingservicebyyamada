package main

import (
	"flag"
	"net/http"
	"os"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/server/handler"
)

func init() {
	flag.Parse()
	if flag.Arg(0) == "prod" {
		os.Setenv("mode", "production")
	} else {
		os.Setenv("mode", "development")
	}
}

func main() {

	http.HandleFunc("/", handler.RootHandler)
	http.HandleFunc("/memo/find", handler.FindMemoHandler)
	http.HandleFunc("/memo/findAll", handler.FindAllMemosHandler)
	http.HandleFunc("/memo/add", handler.AddMemoHandler)
	http.HandleFunc("/memo/update", handler.UpdateMemoHandler)
	http.HandleFunc("/memo/delete", handler.DeleteMemoHandler)
	http.HandleFunc("/book/find", handler.FindBookHandler)
	http.HandleFunc("/book/findAll", handler.FindAllBooksHandler)
	http.HandleFunc("/book/add", handler.AddBookHandler)
	http.HandleFunc("/book/update", handler.UpdateBookHandler)
	http.HandleFunc("/book/delete", handler.DeleteBookHandler)
	http.HandleFunc("/book/suggestions", handler.SuggestionsBookHandler)
	http.HandleFunc("/user/login", handler.LoginHandler)
	http.HandleFunc("/user/logout", handler.LogoutHandler)
	http.HandleFunc("/user/signup", handler.SignUpHandler)
	http.HandleFunc("/user/verify", handler.VerifyHandler)
	http.HandleFunc("/user/findFromSession", handler.FindUserFromSessionHandler)
	http.HandleFunc("/public/stylesheet/prism.css", handler.PrismCSSHandler)
	http.HandleFunc("/public/javascript/prism.js", handler.PrismJsHandler)
	http.HandleFunc("/public/stylesheet/main.css", handler.MainCSSHandler)
	http.HandleFunc("/dist/bundle.js", handler.BundleJsHandler)
	http.HandleFunc("/public/image/book_icon.png", handler.BookIconPNGHandler)
	http.ListenAndServe(":8081", nil)
}
