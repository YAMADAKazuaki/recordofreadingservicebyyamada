package http

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type BookHTTPGoogleRepository struct {
}

func (*BookHTTPGoogleRepository) SuggestionsBook(book model.BookRecord) []model.BookRecord {
	//resp, err := http.Get("http://suggestqueries.google.com/complete/search?client=firefox&printType=books&q=" + book.Title)
	hankakuReplaced := strings.Replace(book.Title, " ", "%20", -1)
	replaced := strings.Replace(hankakuReplaced, "　", "%20", -1)
	resp, err := http.Get("https://www.googleapis.com/books/v1/volumes?printType=books&q=" + replaced)
	if err != nil {
		panic(err)
	}
	body := resp.Body
	responseJSON, err := ioutil.ReadAll(body)
	if err != nil {
		panic(err)
	}
	defer body.Close()

	var jsn model.BookSuggestion
	if err := json.Unmarshal(responseJSON, &jsn); err != nil {
		panic(err)
	}

	var records []model.BookRecord
	for _, s := range jsn.Items {
		var isbn13 string
		for _, i := range s.VolumnInfo.IndustryIdentifiers {
			if i.Type == "ISBN_13" {
				isbn13 = i.Identifier
			}
		}
		var author string
		if len(s.VolumnInfo.Authors) > 0 {
			author = s.VolumnInfo.Authors[0]
		} else {
			author = ""
		}
		var image string
		if len(s.VolumnInfo.ImageLinks.SmallThumnailURL) > 0 {
			image = s.VolumnInfo.ImageLinks.SmallThumnailURL
		} else {
			image = model.NoImageURL
		}
		r := model.BookRecord{
			Title:  s.VolumnInfo.Title,
			Author: author,
			Image:  image,
			ISBN13: isbn13,
		}
		records = append(records, r)
	}
	return records
}
