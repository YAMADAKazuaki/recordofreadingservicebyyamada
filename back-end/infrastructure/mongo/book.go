package mongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type BookMongoRepository struct{}

func (*BookMongoRepository) Find(id string) model.BookRecord {
	client, collection, ctx, cancelFunc := mongoClient("book")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	res := collection.FindOne(ctx, bson.M{"_id": docID})

	book := model.BookRecord{}
	res.Decode(&book)
	return book
}

func (*BookMongoRepository) FindAll(userid string) []model.BookRecord {
	client, collection, ctx, cancelFunc := mongoClient("book")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	cursor, err := collection.Find(ctx, bson.M{"userid": userid})
	if err != nil {
		panic(err)
	}
	defer cursor.Close(ctx)

	bookRows := make([]model.BookRecord, 0)
	for cursor.Next(ctx) {
		result := model.BookRecord{}
		err = cursor.Decode(&result)
		if err != nil {
			panic(err)
		}
		bookRows = append(bookRows, result)
	}
	return bookRows
}

func (*BookMongoRepository) Add(book model.BookRecord) {
	client, collection, ctx, cancelFunc := mongoClient("book")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	_, err := collection.InsertOne(ctx, bson.M{"userid": book.UserID, "title": book.Title, "author": book.Author, "image": book.Image, "isbn13": book.ISBN13})
	if err != nil {
		panic(err)
	}
}

func (*BookMongoRepository) Update(book model.BookRecord) {
	client, collection, ctx, cancelFunc := mongoClient("book")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(book.ID)
	if err != nil {
		panic(err)
	}
	_, err = collection.UpdateOne(
		ctx,
		bson.M{"_id": docID},
		bson.D{
			{Key: "$set", Value: bson.D{{Key: "title", Value: book.Title}}},
			{Key: "$set", Value: bson.D{{Key: "isbn13", Value: book.ISBN13}}},
		},
	)
	if err != nil {
		panic(err)
	}
}

func (*BookMongoRepository) Delete(id string) {
	client, colletion, ctx, cancelFunc := mongoClient("book")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	_, err = colletion.DeleteOne(ctx, bson.M{"_id": docID})
	if err != nil {
		panic(err)
	}
}
