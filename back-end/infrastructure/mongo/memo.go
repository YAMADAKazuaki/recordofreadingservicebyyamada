package mongo

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type MemoMongoRepository struct{}

func (*MemoMongoRepository) Find(id string) model.MemoRecord {
	client, collection, ctx, cancelFunc := mongoClient("memo")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	res := collection.FindOne(ctx, bson.M{"_id": docID})
	mdr := model.MemoRecord{}
	res.Decode(&mdr)
	return mdr
}

func (*MemoMongoRepository) FindAll(userid string, bookid string) []model.MemoRecord {
	client, collection, ctx, cancelFunc := mongoClient("memo")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	cursor, err := collection.Find(ctx, bson.M{"$and": []interface{}{
		bson.M{"userid": userid},
		bson.M{"bookid": bookid}}})
	if err != nil {
		panic(err)
	}
	defer cursor.Close(ctx)

	memoRecords := make([]model.MemoRecord, 0)
	for cursor.Next(ctx) {
		result := model.MemoRecord{}
		err = cursor.Decode(&result)
		if err != nil {
			panic(err)
		}
		memoRecords = append(memoRecords, result)
	}
	return memoRecords
}

func (*MemoMongoRepository) Add(markdown string, userid string, bookid string) {
	client, collection, ctx, cancelFunc := mongoClient("memo")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	day := time.Now()
	_, err := collection.InsertOne(ctx, bson.M{"markdown": markdown, "userid": userid, "bookid": bookid, "created_datetime": day.String()})
	if err != nil {
		panic(err)
	}
}

func (*MemoMongoRepository) Update(id string, md string) {
	client, collection, ctx, cancelFunc := mongoClient("memo")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	_, err = collection.UpdateOne(
		ctx,
		bson.M{"_id": docID},
		bson.D{
			{Key: "$set", Value: bson.D{{Key: "markdown", Value: md}}}},
	)
	if err != nil {
		panic(err)
	}
}

func (*MemoMongoRepository) Delete(id string) {
	client, colletion, ctx, cancelFunc := mongoClient("memo")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	_, err = colletion.DeleteOne(ctx, bson.M{"_id": docID})
	if err != nil {
		panic(err)
	}
}
