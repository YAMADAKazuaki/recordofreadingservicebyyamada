package mongo

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type TokenMongoRepository struct{}

func (repo *TokenMongoRepository) FindByID(id string) model.TokenRecord {
	client, collection, ctx, cancelFunc := mongoClient("token")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	res := collection.FindOne(ctx, bson.M{"_id": docID})
	tokenRecord := model.TokenRecord{}
	res.Decode(&tokenRecord)
	return tokenRecord
}

func (repo *TokenMongoRepository) FindByUserID(userID string) model.TokenRecord {
	client, collection, ctx, cancelFunc := mongoClient("token")
	defer client.Disconnect(ctx)
	defer cancelFunc()

	res := collection.FindOne(ctx, bson.M{"userid": userID})
	tokenRecord := model.TokenRecord{}
	res.Decode(&tokenRecord)
	return tokenRecord
}

func (repo *TokenMongoRepository) Add(userID string) {
	client, collection, ctx, cancelFunc := mongoClient("token")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	day := time.Now()
	_, err := collection.InsertOne(ctx, bson.M{"userid": userID, "created_datetime": day.String()})
	if err != nil {
		panic(err)
	}
}

func (repo *TokenMongoRepository) DeleteByUserID(userID string) {
	client, collection, ctx, cancelFunc := mongoClient("token")
	defer client.Disconnect(ctx)
	defer cancelFunc()

	_, err := collection.DeleteOne(ctx, bson.M{"userid": userID})
	if err != nil {
		panic(err)
	}
}
