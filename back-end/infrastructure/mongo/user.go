package mongo

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type UserMongoRepository struct{}

func (*UserMongoRepository) FindByID(id string) model.UserRecord {
	client, collection, ctx, cancelFunc := mongoClient("user")
	defer client.Disconnect(ctx)
	defer cancelFunc()

	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		panic(err)
	}
	res := collection.FindOne(ctx, bson.M{"_id": docID})
	userRecord := model.UserRecord{}
	res.Decode(&userRecord)
	return userRecord
}

func (*UserMongoRepository) FindByEmail(email string) model.UserRecord {
	client, collection, ctx, cancelFunc := mongoClient("user")
	defer client.Disconnect(ctx)
	defer cancelFunc()

	res := collection.FindOne(ctx, bson.M{"email": email})
	usrrcd := model.UserRecord{}
	res.Decode(&usrrcd)
	return usrrcd
}

func (*UserMongoRepository) Verify(id string) error {
	client, collection, ctx, cancelFunc := mongoClient("user")
	defer client.Disconnect(ctx)
	defer cancelFunc()

	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return err
	}
	_, err = collection.UpdateOne(
		ctx,
		bson.M{"_id": docID},
		bson.D{
			{Key: "$set", Value: bson.D{{Key: "is_verified", Value: true}}},
		},
	)
	if err != nil {
		return err
	}
	return nil
}

func (*UserMongoRepository) Add(record model.UserRecord) {
	client, collection, ctx, cancelFunc := mongoClient("user")
	defer client.Disconnect(ctx)
	defer cancelFunc()
	_, err := collection.InsertOne(ctx, bson.M{
		"userid":      record.ID,
		"name":        record.Name,
		"email":       record.Email,
		"password":    record.Password,
		"is_verified": record.IsVerified,
		"token":       record.Token,
	})
	if err != nil {
		panic(err)
	}
}
