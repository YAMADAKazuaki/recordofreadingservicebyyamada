package mongo

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/BurntSushi/toml"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/util"
)

var config util.Config
var connectionURL string

func init() {
	mode := os.Getenv("mode")
	var path string
	if len(mode) > 0 {
		path = mode + ".tml"
	} else {
		path = "development.tml"
	}
	_, err := toml.DecodeFile(path, &config)
	if err != nil {
		panic(err)
	}

	connectionURL = fmt.Sprintf("mongodb://%v:%v@%v:%v", config.Server.RootUserName, config.Server.RootPassword, config.Server.Host, config.Server.Port)
}

func mongoClient(collectionName string) (*mongo.Client, *mongo.Collection, context.Context, context.CancelFunc) {

	client, err := mongo.NewClient((options.Client().ApplyURI(connectionURL)))
	if err != nil {
		panic(err)
	}
	ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		panic(err)
	}
	collection := client.Database(config.Db.Database).Collection(collectionName)
	return client, collection, ctx, cancelFunc
}
