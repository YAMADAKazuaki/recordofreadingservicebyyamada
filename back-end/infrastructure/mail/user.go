package mail

import (
	"fmt"
	"net/smtp"

	"gitlab.com/yamadakazuaki/recordofreadingservicebyyamada/domain/model"
)

type UserEmailGmailRepository struct {
}

type mail struct {
	From     string
	UserName string
	Password string
	To       string
	Subject  string
	Message  string
}

func (m mail) Body() string {
	return "To: " + m.To + "\r\n" +
		"Subject: " + m.Subject + "\r\n\r\n" +
		m.Message + "\r\n"
}

func (*UserEmailGmailRepository) SendMail(user model.UserRecord) {
	m := mail{
		From:     "noreply.iterable.corp@gmail.com",
		UserName: "noreply.iterable.corp@gmail.com",
		Password: "n0reply1",
		To:       user.Email,
		Subject:  "メールアドレス認証",
		Message:  fmt.Sprintf(`<html><a href="http://localhost:8081/user/verify?token=%s&userid=%s">クリックしてください。</a></html>`, user.Token, user.ID),
	}
	smtpSvr := "smtp.gmail.com:587"
	auth := smtp.PlainAuth("", m.UserName, m.Password, "smtp.gmail.com")
	err := smtp.SendMail(smtpSvr, auth, m.From, []string{m.To}, []byte(m.Body()))
	if err != nil {
		panic(err)
	}
}
